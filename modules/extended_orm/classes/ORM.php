<?php

defined('SYSPATH') OR die('No direct script access.');

class ORM extends Kohana_ORM {

    /**
     * Table secondary key
     * @var string
     */
    protected $_secondary_key = 'guid';

    /**
     * Secondary key value
     * @var mixed
     */
    protected $_secondary_key_value;

    public function __construct($id = NULL) {
        $this->_initialize();

        if ($id !== NULL) {
            if (is_array($id)) {
                foreach ($id as $column => $value) {
                    // Passing an array of column => values
                    $this->where($column, '=', $value);
                }

                $this->find();
            } else {
                // Passing the primary key
                if (isset($this->_table_columns["guid"])) {
                    $this->
                            where($this->_object_name . '.' . $this->_primary_key, '=', $id)->
                            or_where($this->_object_name . '.' . $this->_secondary_key, '=', $id)->
                            find();
                } else {
                    $this->
                            where($this->_object_name . '.' . $this->_primary_key, '=', $id)->
                            find();
                }
            }
        } elseif (!empty($this->_cast_data)) {
            // Load preloaded data from a database call cast
            $this->_load_values($this->_cast_data);

            $this->_cast_data = array();
        }
    }

    public function create(Validation $validation = NULL) {

        if (isset($this->_table_columns["guid"])) {
            $this->guid = DB::expr("UUID()");
        }
        if (isset($this->_table_columns["created_ts"])) {
            $this->created_ts = DB::expr("NOW()");
        }
        parent::create($validation);
    }

    public function update(Validation $validation = NULL) {
        if (isset($this->_table_columns["updated_ts"])) {
            $this->updated_ts = DB::expr("NOW()");
        }
        parent::update($validation);
    }

    public function display() {
        return $this->name;
    }

    public function get_og_name() {
        return $this->name;
    }

    public function to_ajax() {
        $data = array(
            "guid" => $this->guid,
            "name" => $this->name,
            "display" => $this->display(),
        );
        return $data;
    }

}
