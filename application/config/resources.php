<?php

defined('SYSPATH') OR die('No direct access allowed!');

return array(
    'scripts' => array(
        'application' => array(
            'footer' => array(),
            'body' => array(),
            'head' => array()
        )
    ),
    'styles' => array(
        'media/css/vendor/all.css',
        'media/css/app/app.css',
        'media/css/todo.css',
    )
);
