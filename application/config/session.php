<?php

return array(
    'native' => array(
        'name' => 'todooappssession',
        'lifetime' => 0
    ),
    'cookie' => array(
        'name' => 'todoappscoockie',
        'encrypted' => TRUE,
        'lifetime' => 0,
    ),
    'todo' => array(
        'name' => 'todoappscoockie',
        'lifetime' => 0,
    ),
);
