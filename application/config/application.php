<?php

defined('SYSPATH') OR die('No direct access allowed.');

return array
    (
    'app' => array
        (
        'url' => "",
        'version' => array(
            'name' => 'Adora Belle Dearheart',
            'number' => '1.0',
        ),
        'meta' => array(
            'title' => 'To Do App',
            'name' => 'To Do App',
            'description' => 'todo API Applications',
        ),
    ),
);
