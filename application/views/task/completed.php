<tr>
    <td  width="2%"> 
        <a href="<?= URL::site("task/{$task->guid}/active") ?>">
            <img height="30px" width="30px" src='http://dev.todoapp.com/media/img/checked.png' >
        </a>
    </td>
    <td   class="text-left" colspan="13">
        <a class="completed">
            <?= $task->title ?>
        </a>
    </td>
    <td class="text-right">
        <a href="<?= URL::site("task/{$task->guid}/delete") ?>" class="btn btn-x-btn">
            <i class="material-icons">close</i>
        </a>
    </td>
</tr>
