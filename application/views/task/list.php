<table class="table table-hover">
    <thead>
        <tr>
    <form action="<?= URL::site("task/new") ?>" method="post">
        <th width="2%">

            <button type="submit" onclick="newTask()" class="btn btn-btn"><i class="fa fa-angle-down" aria-hidden="true"></i></button>
        </th>
        <th colspan="15">

            <input type="text" class="form-input" name="newTask" id="newInput" placeholder="What needs to be done ? "/>
        </th>
    </form>
</form>
</tr>
</thead>
<tbody>
    <?php
    foreach ($tasks as $task) {

        if ($task->status == "active" || $task->status == "completed") {
            print View::factory("task/{$task->status}", array(
                        'task' => $task
            ));
        }
    }
    if (count($tasks) == 0) {
        print "<td colspan='15'> $message</td>";
    }
    ?>
</tbody>
<tfoot>
    <tr>
        <td colspan="15"> 
            <div class="items col-xs-3 col-sm-3 col-md-3 col-lg-3"><?= $activeItems . " items left" ?></label>
            </div>
            <div class="tabs col-xs-9 col-sm-9 col-md-9 col-lg-9">
                <a href="<?= URL::site('task/list') ?>" class="btn a-btn" >All</a>
                <a href="<?= URL::site('task/active') ?>" class="btn a-btn">Active</a>
                <a href="<?= URL::site('task/completed') ?>" class="btn a-btn">Completed</a>
            </div>
        </td>
    </tr>
</tfoot>
</table>

<!--<script>
    function newTask() {
        var newInput = document.getElementById("newInput").value;
        console.log(newInput);

        if (newInput === "" && newInput === " ") {
            alert("You must write task!");
        }
    }

</script>-->

<style>

    a.completed{
        color: #D5D5D5;
        text-decoration: line-through;
    }
    a.completed:hover{
        color: #D5D5D5;
        text-decoration: line-through;
    }
    a.active{
        color: #6D6B6B;
        text-decoration: none;
    }
    a.active:hover{
        color: #6D6B6B;
        text-decoration: none;
    }

    tbody{
        color: #858585;
        font-style: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: x-large;
        line-height: normal;
        font-family: sans-serif;
        line-height: inherit;

    }
    .items{
        color: #858585;
        padding: 6px 12px;
        font-size: 14px;
    }
    .form-input:valid {
        font-style: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: x-large;
        line-height: normal;
        font-family: sans-serif;
        line-height: inherit;
        border: none
    }
    ::-webkit-input-placeholder {
        color: #E0DDDD;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #E0DDDD;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
        color: #E0DDDD;  
    }

    :-ms-input-placeholder {  
        color: #E0DDDD;  
    }
    .a-btn{
        color: #858585;
    }

    .a-btn:hover {
        color:#858585;
        border:solid 1px;
        border-color: #E0DDDD;
    }
    .btn-btn{
        background-color: transparent;
        color: #E0DDDD;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: 34px;
        line-height: 1;
        font-family: FontAwesome;
    }

    .btn-btn:hover {
        color: #000;
    }
    .btn-x-btn{
        padding: 0px 12px;
        background-color: transparent;
        color: #A00A0A;
    }
    .btn-x-btn:hover {
        color: #FF0000;
    }
</style>