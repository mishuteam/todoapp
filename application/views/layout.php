<?php
defined('SYSPATH') or die('No direct script access.');
?>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar ls-bottom-footer" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link href="http://dev.todoapp.com/media/css/todo.css" rel="stylesheet">-->
        <link href="http://dev.todoapp.com/media/css/vendor/all.css" rel="stylesheet">
        <link href="http://dev.todoapp.com/media/css/app/app.css" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="shortcut icon" href="http://dev.todoapp.com/media/img/checked.png">

    </head>
    <body>
        <div class="body">
            <?php
            echo $content;
            ?>
        </div>
        <footer class="footer">
            <strong><?= $meta['name'] ?></strong> v<?= $version['number'] ?> © Copyright <?= date("Y") ?>
        </footer>

    </body>
</html>


<style>
    body{
        background-color: white;
    }
</style>