<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Helper message
 *
 * @author Mihail Davidov
 * Created on 2017-01-15
 */
class Helpers {

//    const DBFORMAT = "Y-m-d";
//    const VIEWFORMAT = "d-m-Y";
//    const INPUT_DATE_FORMAT = "dd-MM-yyyy";
//    const INPUT_TS_FORMAT = "H:i:s dd-MM-yyyy";
//    const TSDBFORMAT = 'Y-m-d H:i:s';
//    const TSVIEWFORMAT = 'd-m-Y H:i';
//    const TO_TODAY = "today";
//    const TO_NULL = "";
//    const TO_START = "start";
//

    public static function uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public static function vardump($var, $title = NULL) {
        print "<pre>";
        print (!is_null($title) ? "<h1>{$title}</h1>" : "");
        var_dump($var);
        print "</pre>";
    }

//    public static function parse_date($date, $format, $to = Helpers::TO_NULL) {
//        if (($date == "0000-00-00") or ( $date == "0000-00-00 00:00:00")) {
//            switch ($to) {
//                case self::TO_NULL:
//                    return "";
//                    break;
//                case self::TO_START:
//                    $time = 0;
//                    break;
//                case self::TO_TODAY:
//                    $time = time();
//                    break;
//            }
//        } else {
//            $time = strtotime($date);
//        }
//        $newFormat = date($format, $time);
//        return $newFormat;
//    }
//
//    public static function today_date($format) {
//
//        $today = date($format);
//        return $today;
//    }
//
//    public static function next_week_day($format) {
//
//        $nextWeek = date($format, strtotime('+1 week'));
//        return $nextWeek;
//    }
//
//    public static function today_plustree($format) {
//
//        $today_plustree = date($format, strtotime('+3 days'));
//        return $today_plustree;
//    }
//    public static function checkUnique($model, $param, $value = NULL) {
//        if (is_array($param)) {
////            $obj = ORM::factory($model);
//            $count = 0;
//            foreach ($param as $key => $value) {
//                if ($count == 0) {
//                    $obj->where($key, "=", $value);
//                } else {
//                    $obj->and_where($key, "=", $value);
//                }
//                $count++;
//            }
//            $check = $obj->find();
//        } else {
////            $check = ORM::factory($model)->where($param, "=", $value)->find();
//        }
//        return $check->loaded();
//    }
//    public static function get_media_destination_path($data_type, $owner_type, $owner_guid) {
//        //$image_path = SITEPATH."img/user/{$this->profile->guid}/";
//        return SITEPATH . "{$data_type}/{$owner_type}/{$owner_guid}/";
//    }
//
//    public static function create_media_destination_path($path) {
//        $parts = explode(DIRECTORY_SEPARATOR, $path);
//        $new_path = "";
//        foreach ($parts as $part) {
//            if ($part != "") {
//                $new_path = $new_path . DIRECTORY_SEPARATOR . $part;
//                if (!file_exists($new_path)) {
//                    mkdir($new_path);
//                }
//            }
//        }
//    }
//
//    public static function redirect($url) {
//        return URL::site($url);
//    }
//
//    public static function debug_route() {
//        $request = Kohana_Request::detect_uri();
//        var_dump($request);
//        $q = Request::factory("/" . $request);
//        print "<pre>";
//        foreach (Route::all() as $k => $r) {
//            var_dump($k);
//            var_dump($r->matches($q));
//        }
//        print "</pre>";
//    }
//    public static function action_upload($fieldName, $directory) {
//        $error_message = NULL;
//        $filename = NULL;
//        if (isset($_FILES[$fieldName])) {
//            $filename = self::_save_image($_FILES[$fieldName], $directory);
//        }
//
//        if (!$filename) {
//            $error_message = 'There was a problem while uploading the image.
//                Make sure it is uploaded and must be JPG/PNG/GIF file.';
//            throw new Exception($error_message);
//        }
//        return $filename;
//    }
//    protected static function _save_image($image, $directory) {
//        if (
//                !Upload::valid($image) OR ! Upload::not_empty($image) OR ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))) {
//            return FALSE;
//        }
//        $directory = self::check_path($directory) . "/";
//        if ($file = Upload::save($image, NULL, MEDIA . $directory)) {
//            $pathinfo = pathinfo($file);
//            $rand = Text::random('alnum', 3);
//            $sizes = Kohana::$config->load('application.sizes');
//            $filename = "{$pathinfo['filename']}_{$rand}.{$pathinfo['extension']}";
//            rename($file, MEDIA . $directory . $filename);
//            $file = $filename;
//            foreach ($sizes as $type) {
//                foreach ($type as $size) {
//                    $path = self::check_path($directory . "/" . $size["h"] . "/" . $size["w"]) . "/";
//                    $filename = "{$pathinfo['filename']}_{$rand}.{$pathinfo['extension']}";
//                    Image::factory(MEDIA . $directory . $file)
//                            ->resize($size["w"], $size["h"], Image::AUTO)
//                            ->save(MEDIA . $path . $filename);
//                }
//            }
//            return $file;
//        }
//        return FALSE;
//    }
//
//    public static function check_path($path) {
//        $parts = explode('/', $path);
//        $test = substr(MEDIA, 0, -1);
//        foreach ($parts as $value) {
//            if ($value != "") {
//                $test = $test . "/" . $value;
//                if (!file_exists($test)) {
//                    mkdir($test, 0777);
//                }
//            }
//        }
//        return $path;
//    }
//
//    public static function select_type($object, $column) {
//        if (!$column_info = Arr::get($object->table_columns(), $column)) {
//            return array();
//        }
//
//        $options = $column_info['options'];
//
//        return $options;
//    }
//    public static function generateRandomString($length = 10) {
//        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//        $charactersLength = strlen($characters);
//        $randomString = '';
//        for ($i = 0; $i < $length; $i++) {
//            $randomString .= $characters[rand(0, $charactersLength - 1)];
//        }
//        return $randomString;
//    }
//
//    public static function destination_sizes($original_width, $original_height, $destination_width, $destination_height) {
//        $original_ratio = $original_width / $original_height;
//        if ((is_null($destination_height)) or ( $destination_height == "")) {
//            $destination_height = round($original_height * $destination_width / 100);
//        }
//        $destination_ratio = $destination_width / $destination_height;
//        if ($original_ratio != $destination_ratio) {
//            $check_height = $destination_width * $original_height / $original_width;
//            if ($check_height > $destination_height) {
//                $check_width = $destination_height * $original_width / $original_height;
//                $destination_width = round($check_width);
//            } else {
//                $destination_height = round($check_height);
//            }
//        }
//        return array(
//            "w" => $destination_width,
//            "h" => $destination_height
//        );
//    }
//
//    public static function curl_post($url, $json_arr) {
//        $json = json_encode($json_arr);
//        $new = str_replace(' ', '%20', $url);
//
//        $ch = curl_init($new);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($json)
//        ));
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
//        $result = curl_exec($ch);
//
//        curl_close($ch);
//        return $result;
//    }
//
//    public static function curl_get($url) {
//        $new = str_replace(' ', '%20', $url);
//
//        $ch = curl_init($new);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//
//        $result = curl_exec($ch);
//
//        curl_close($ch);
//        return $result;
//    }
}

?>
