<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Template {

    public $title = "";
    public $session = null;
    public $directory = null;
    public $action = null;
    public $controller = null;
    public $meta = array();
    public $styles = array();
    public $template = 'layout';

    public function before() {
        $this->action = $this->request->action();
        $this->controller = $this->request->controller();
        parent::before();
    }

    public function after() {
        if ($this->auto_render === TRUE) {
            View::set_global('tasks', array());
            View::set_global('meta', array_merge(Kohana::$config->load('application.app.meta'), $this->meta));
            View::set_global('version', Kohana::$config->load('application.app.version'));
            View::set_global('directory', $this->directory);
            View::set_global('controller', $this->controller);
            View::set_global('action', $this->action);
            $this->template->title = ($this->title != "" ? (ucwords($this->title) . ' | ') : "") . Kohana::$config->load('application.app.meta.title');
            $this->template->styles = array_merge(Kohana::$config->load('resources.styles'), $this->styles);
        } else {
            $this->response->headers("Cache-Control", "no-store, no-cache, must-revalidate");
            $this->response->headers("Pragma", "no-cache");
            $this->response->headers("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
            $this->response->headers("Content-type", "application/json");
        }
        parent::after();
    }

}
