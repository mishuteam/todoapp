<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Task extends Controller_Main {

    public function before() {
        $this->activeTasks = ORM::factory("Task")->where("status", "=", "active")->find_all();
        $this->activeItems = count($this->activeTasks);
        $this->compleatedTasks = ORM::factory("Task")->where("status", "=", "completed")->find_all();
        $this->message = "The task list is empty !";
        parent::before();
    }

    public function action_index() {
        $this->redirect("task/list");
    }

    public function action_list() {


        $tasks = ORM::factory("Task")->find_all();

        $this->template->content = View::factory('task/list', array(
                    'tasks' => $tasks,
                    'activeItems' => $this->activeItems,
                    'message' => $this->message,
        ));
    }

    public function action_new() {

        if ($this->request->method() == Request::POST) {

            if ($this->request->post('newTask') != '') {  // i need to chek if is given only &nbsp; so we dont save empty task;
                $newTask = new Model_Task;
                $newTask->title = $this->request->post('newTask');
                $newTask->guid = Helpers::uuid();
                $newTask->save();
            }
            $this->redirect("task/list");
        } else {

            $this->template->content = View::factory('task/list', array(
                        'tasks ' => $this->activeTasks,
                        'activeItems' => $this->activeItems,
                        'message' => $this->message,
            ));
        }
    }

    public function action_active() {
        if ($this->request->param("guid") != "") {
            $task = ORM::factory("Task")->where("guid", "=", $this->request->param("guid"))->find();
            $task->status = "active";
            $task->save();
            $this->redirect("task/list");
        } else {
            $this->template->content = View::factory('task/list', array(
                        'tasks' => $this->activeTasks,
                        'activeItems' => $this->activeItems,
                        'message' => $this->message,
            ));
        }
    }

    public function action_completed() {

        if ($this->request->param("guid") != "") {
            $task = ORM::factory("Task")->where("guid", "=", $this->request->param("guid"))->find();

            $task->status = "completed";
            $task->save();
            $this->redirect("task/list");
        } else {
            $this->template->content = View::factory('task/list', array(
                        'activeItems' => $this->activeItems,
                        'tasks' => $this->compleatedTasks,
                        'message' => $this->message,
            ));
        }
    }

    // hire we are deleting permanently the task we won to delete
    public function action_delete() {
        if ($this->request->param("guid") != "") {
            $task = ORM::factory("Task")->where("guid", "=", $this->request->param("guid"))->find();
            $task->delete();
            $this->redirect("task/list");
        } else {
            $this->redirect("task/list");
        }
    }

    // hire we are changing the status of the task we won to delete, and it is stil in the database if we won to see deleted task 
    public function action_delete2() {

        if ($this->request->param("guid") != "") {
            $task = ORM::factory("Task")->where("guid", "=", $this->request->param("guid"))->find();
            $task->status = "deleted";
            $task->save();
            $this->redirect("task/list");
        } else {
            $this->redirect("task/list");
        }
    }

}
