<?php

defined('SYSPATH') or die('No direct script access.');

Route::set('routes', '(<controller>(/<guid>(/<action>(/<overflow>))))', array('overflow' => '.*?'))
        ->filter(function($route, $params, $request) {
            $data['directory'] = '';
            $data['controller'] = isset($params['controller']) ? ucfirst($params['controller']) : "Task";
            $overflow = isset($params['overflow']) ? $params['overflow'] : "";
            if (isset($params['guid'])) {
                if (strlen($params['guid']) == 36) {
                    $data['guid'] = $params['guid'];
                    $data['action'] = isset($params['action']) ? $params['action'] : "index";
                } else {
                    $data['action'] = $params['guid'];
                    if (isset($params['action'])) {
                        $overflow = $params['action'] . ($overflow != "" ? ("/" . $overflow) : "");
                    }
                }
            } else {
                $data['action'] = "index";
            }
            $data['overflow'] = $overflow;
            return $data;
        })
        ->defaults(array(
            'directory' => 'todo',
            'controller' => 'Task',
            'action' => 'index',
        ));
//Route::set('routes', '(<directory>(/<controller>(/<guid>(/<action>(/<overflow>)))))', array(
//    'directory' => 'todo',
//    'overflow' => '.*?'
//))->filter(function($route, $params, $request) {
//    $data = $params;
//    $overflow = isset($params['overflow']) ? $params['overflow'] : "";
//    if (isset($params['directory'])) {
//        $directory = ucfirst(Inflector::singular($params['directory']));
//        if (isset($params['controller'])) {
//            $controller = ucfirst(Inflector::singular($params['controller']));
//            if (class_exists("Controller_" . $directory . "_" . $controller)) {
//                $data['directory'] = $directory;
//                $data['controller'] = $controller;
//            } else if (class_exists("Controller_" . $directory)) {
//                $data['directory'] = '';
//                $data['controller'] = $directory;
//                if ($params['controller'] != 'Welcome') {
//                    $data['action'] = strtolower($params['controller']);
//                }
//            } else if (class_exists("Controller_" . $controller)) {
//                $data['directory'] = '';
//                $data['controller'] = $controller;
//            } else {
//                $data['action'] = $params['directory'];
//            }
//            if (isset($params['guid'])) {
//                if (strlen($params['guid']) == 36) {
//                    $data['guid'] = $params['guid'];
//                    if ((isset($params['action'])) and ( strlen($params['action']) == 36)) {
//                        $data['child_guid'] = $params['action'];
//                        if (isset($params['overflow'])) {
//                            $params = explode("/", $params['overflow']);
//                            $data['action'] = $params[0];
//                            unset($params[0]);
//                            $overflow = implode("/", $params);
//                        } else {
//                            $data['action'] = "index";
//                        }
//                    } else {
//                        $data['action'] = isset($params['action']) ? $params['action'] : "index";
//                    }
//                } else {
//                    $data['action'] = $params['guid'];
//                    if (isset($params['action'])) {
//                        $overflow = $params['action'] . ($overflow != "" ? ("/" . $overflow) : "");
//                    }
//                }
//            }
//        } else {
//            if (class_exists("Controller_" . $directory)) {
//                $data['directory'] = '';
//                $data['controller'] = $directory;
//            }
//        }
//    }
//
//    return $data;
//})->defaults(array(
//    'directory' => '',
//    'controller' => 'App',
//    'action' => 'index',
//));
