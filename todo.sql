-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 14, 2017 at 12:57 AM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: "todo"
--
CREATE DATABASE IF NOT EXISTS "todo" DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE todo;

-- --------------------------------------------------------

--
-- Table structure for table "tasks"
--

DROP TABLE IF EXISTS `tasks`;
CREATE TABLE "tasks" (
  "id" int NOT NULL,
  "guid" varchar(36) NOT NULL,
  "title" varchar(255) NOT NULL,
  "status" enum('active','completed','deleted') NOT NULL DEFAULT 'active',
  "created_ts" timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_ts" timestamp NULL DEFAULT NULL,
  PRIMARY KEY ("id")
);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
